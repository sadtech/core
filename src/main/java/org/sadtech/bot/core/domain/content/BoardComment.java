package org.sadtech.bot.core.domain.content;

public class BoardComment extends Comment {

    public BoardComment() {
        type = ContentType.BOARD_COMMENT;
    }

}
