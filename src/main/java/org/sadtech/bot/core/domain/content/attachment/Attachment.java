package org.sadtech.bot.core.domain.content.attachment;

public abstract class Attachment {

    protected AttachmentType type;

    public AttachmentType getType() {
        return type;
    }

}
