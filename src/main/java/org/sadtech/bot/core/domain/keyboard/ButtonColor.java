package org.sadtech.bot.core.domain.keyboard;

public enum ButtonColor {

    PRIMARY, DEFAULT, NEGATIVE, POSITIVE

}
