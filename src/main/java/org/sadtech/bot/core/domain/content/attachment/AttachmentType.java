package org.sadtech.bot.core.domain.content.attachment;

public enum AttachmentType {

    AUDIO_MESSAGE, GEO

}
