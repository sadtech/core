package org.sadtech.bot.core.domain.money;

public enum AccountStatus {

    EXPOSED, CLOSED, CANCELLED, EXCEPTION

}
