package org.sadtech.bot.core.domain.content;

public enum ContentType {

    MAIL, BOARD_COMMENT, EMPTY

}
