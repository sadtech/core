package org.sadtech.bot.core.service;

import org.sadtech.bot.core.domain.content.BoardComment;

public interface BoardCommentService extends ContentService<BoardComment> {
}
