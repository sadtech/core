package org.sadtech.bot.core.service;

import org.sadtech.bot.core.domain.content.Mail;

public interface MailService extends ContentService<Mail> {

}
